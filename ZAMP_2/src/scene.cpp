/*
 * scene.cpp
 *
 *  Created on: 4 gru 2015
 *      Author: dawid
 */
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/XMLString.hpp>

#include <iostream>
#include "xmlparser4scene.hh"
#include "scene.hh"

using namespace std;
using namespace xercesc;

/*!
 * Czyta z pliku opis sceny i zapisuje stan sceny do parametru,
 * który ją reprezentuje.
 * \param sFileName - (\b we.) nazwa pliku z opisem poleceń.
 * \retval true - jeśli wczytanie zostało zrealizowane poprawnie,
 * \retval false - w przeciwnym przypadku.
 */
bool Scene::ReadFile(const char* sFileName)
{
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cerr << "Error during initialization! :\n";
		cerr << "Exception message is: \n"
				<< message << "\n";
		XMLString::release(&message);
		return 1;
	}

	SAX2XMLReader* pParser = XMLReaderFactory::createXMLReader();

	pParser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
	pParser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	pParser->setFeature(XMLUni::fgXercesDynamic, false);
	pParser->setFeature(XMLUni::fgXercesSchema, true);
	pParser->setFeature(XMLUni::fgXercesSchemaFullChecking, true);

	pParser->setFeature(XMLUni::fgXercesValidationErrorAsFatal, true);

	DefaultHandler* pHandler = new XMLParser4Scene(*this);
	pParser->setContentHandler(pHandler);
	pParser->setErrorHandler(pHandler);

	try {

		if (!pParser->loadGrammar("grammar/opis_sceny.xsd",
				xercesc::Grammar::SchemaGrammarType,true)) {
			cerr << "!!! Plik grammar/opis_sceny.xsd, '" << endl
					<< "!!! ktory zawiera opis gramatyki, nie moze zostac wczytany."
					<< endl;
			return false;
		}
		pParser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse,true);
		pParser->parse(sFileName);
	}
	catch (const XMLException& Exception) {
		char* sMessage = XMLString::transcode(Exception.getMessage());
		cerr << "Informacja o wyjatku: \n"
				<< "   " << sMessage << "\n";
		XMLString::release(&sMessage);
		return false;
	}
	catch (const SAXParseException& Exception) {
		char* sMessage = XMLString::transcode(Exception.getMessage());
		char* sSystemId = xercesc::XMLString::transcode(Exception.getSystemId());

		cerr << "Blad! " << endl
				<< "    Plik:  " << sSystemId << endl
				<< "   Linia: " << Exception.getLineNumber() << endl
				<< " Kolumna: " << Exception.getColumnNumber() << endl
				<< " Informacja: " << sMessage
				<< endl;

		XMLString::release(&sMessage);
		XMLString::release(&sSystemId);
		return false;
	}
	catch (...) {
		cout << "Zgloszony zostal nieoczekiwany wyjatek!\n" ;
		return false;
	}

	delete pParser;
	delete pHandler;
	return true;
}

/*!
 * \brief Metoda pozwalająca na zapamiętanie pozycji robota
 *
 * \param[in] x - pozycja robota w osi x w mm
 * \param[in] y - pozycja robota w osi y w mm
 * \param[in] angle - kąt obrotu robota w stopniach
 */
void Scene::setRobotPose(double x, double y, double angle)
{
	robot_x_pose = x;
	robot_y_pose = y;
	robot_angle_pose = angle;
}

/*!
 * \brief Metoda pobierająca zapamiętaną pozycję robota
 *
 * \param[out] x - pozycja robota w osi x w mm
 * \param[out] y - pozycja robota w osi y w mm
 * \param[out] angle - kąt obrotu robota w stopniach
 */
void Scene::getRobotPose(double &x, double &y, double &angle)
{
	x =robot_x_pose;
	y =robot_y_pose;
	angle = robot_angle_pose;
}
/*!
 * \brief Metoda dodająca obiekt sceny do wektora
 *
 * \param[in] Object - obiekt sceny
 */
void Scene::addObject(SceneObject *Object)
{
	ObjectVector.push_back(*Object);
}

/*!
 * \brief Metoda zwracająca wielkość wektora obiektów
 *
 * \return wielkość wektora obiektów
 */
int Scene::objectVectorSize()
{
	return ObjectVector.size();
}
