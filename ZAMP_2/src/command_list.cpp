/*
 * funkcje.cpp
 *
 *  Created on: 8 lis 2015
 *      Author: dawid
 */

#include <iostream>
#include <cstdio>
#include <sstream>
#include <string>
#include "command_list.hh"
#include "RobotMobilny.hh"
#include "lacze_do_gnuplota.hh"
#include "object.hh"

using namespace std;

/*!
 * \brief Metoda wczytująca sekwencje ruchów z pliku
 *
 * Odpytuje użytkownika o podanie ścieżki do pliku. Następnie za pomocą
 * polecenia "cpp -P" otwiera strumień danych i rozpoczyna parsowanie poleceń.
 * Metoda sprawdza poprawność instrukcji i w przypadku gdy wystąpi jakiś błąd
 * użytkownik dostaje informacje która linia jest niepoprawna.
 *
 * \param[in] - Klasa zawierająca listę pluginów
 */
void CommandList::WczytajSekwencje(PluginList *lista_pluginow)
{
	FILE *fp;
	string Cmd = "cpp -P ";
	char nazwa_pliku[100];
	int i = 0;
	char linia[100];
	char nazwa_pluginu[10];
	cout << endl << "Podaj nazwe pliku do wczytania: ";
	cin >> nazwa_pliku;
	Cmd += nazwa_pliku;
	fp = popen(Cmd.c_str(),"r");

	if(!fp) return;

	while (fgets(linia,100,fp))
	{
		stringstream TmpStrm;
		++i;
		TmpStrm << linia;
		TmpStrm >> nazwa_pluginu;
		if(!lista_pluginow->WyszukajPlugin(nazwa_pluginu))
		{
			cmd_ptr cmd(lista_pluginow->UtworzKomende(nazwa_pluginu));
			if(cmd->ReadParams(TmpStrm))
				command_list.push_back(move(cmd));
			else
				cout << endl << "Błąd w skladni polecenia nr " << i << "!" << endl;
		}
		else
			cout << endl << "Brak obslugi polecenia nr " << i << "(" << nazwa_pluginu << ")!" << endl;
	}
}

/*!
 * \brief Przeciąrzenie metody wczytującej sekwencje ruchów z pliku
 *
 * Pozwala na wczytanie nazwy jako parametru
 *
 * \param[in] lista_pluginow - Klasa zawierająca listę pluginów
 * \param[in] nazwa_pliku - nazwa pliku komend
 */
void CommandList::WczytajSekwencje(PluginList *lista_pluginow, const char *nazwa_pliku)
{
	FILE *fp;
	string Cmd = "cpp -P ";
	int i = 0;
	char linia[100];
	char nazwa_pluginu[10];
	Cmd += nazwa_pliku;
	fp = popen(Cmd.c_str(),"r");

	if(!fp) return;

	while (fgets(linia,100,fp))
	{
		stringstream TmpStrm;
		++i;
		TmpStrm << linia;
		TmpStrm >> nazwa_pluginu;
		if(!lista_pluginow->WyszukajPlugin(nazwa_pluginu))
		{
			cmd_ptr cmd(lista_pluginow->UtworzKomende(nazwa_pluginu));
			if(cmd->ReadParams(TmpStrm))
				command_list.push_back(move(cmd));
			else
				cout << endl << "Błąd w skladni polecenia nr " << i << "!" << endl;
		}
		else
			cout << endl << "Brak obslugi polecenia nr " << i << "(" << nazwa_pluginu << ")!" << endl;
	}
}

/*!
 * \brief Metoda wyświetla sekwencje wczytanych ruchów
 */
void CommandList::PokazSekwencje()
{
	if(command_list.begin() == command_list.end())
		cout << endl << "Brak sekwencji ruchu do wyswietlenia!" << endl;
	else
		for(auto it = command_list.begin(); it != command_list.end(); ++it)
			it->get()->PrintCmd();
}

/*!
 * \brief Metoda wykonująca sekwencje ruchów
 *
 * Wykonywane ruchy są wizualizowane za pomocą programu gnuplot oraz
 * zapisywane są do pliku sciezka_robota.dat
 */
void CommandList::WykonajSekwencje(Scene *scena)
{
	if(command_list.begin() == command_list.end())
		cout << endl << "Brak sekwencji ruchu do wyswietlenia!" << endl;
	else
	{
		PzG::LaczeDoGNUPlota  Lacze;
		RobotMobilny  Robot(&Lacze);
		SceneObject *obiekt;
		double x,y,alpha;

		Lacze.Inicjalizuj();
		Lacze.ZmienTrybRys(PzG::TR_2D);

		scena->getRobotPose(x,y,alpha);
		Robot.Set(x,y,alpha);

		for(int i = 0; scena->objectVectorSize() > i; ++i)
		{
			obiekt = scena->getObject(i);
			Lacze.DodajNazwePliku((obiekt->getName()+".dat").c_str(),PzG::RR_Punktowy,obiekt->getSize());
		}

		Lacze.UstawZakresY(-100,2000);
		Lacze.UstawZakresX(-100,2000);

		Robot.Init();

		auto it = command_list.begin();

		while(it!=command_list.end())
		{
			if(!strcmp(it->get()->GetCmdName(),"Grasper"))
			{
				if(Robot.GetGrasper())
					Robot.Loose();
				else
					Robot.Catch(scena);
			}
			it->get()->ExecCmd(&Robot);
			command_list.pop_front();
			it = command_list.begin();
		}
	}
}

/*!
 * \brief Metoda służąca do sprawdzania czy podana komenda znajduje się na liście
 *
 * \param[in] nazwa_polecenia - nazwa wyszukiwanego polecenia
 *
 * \retval 1 - brak podanego polecenia na liście
 * \retval 0 - polecenie znajduje się na liscie
 */
int CommandList::WyszukajKomende(char *nazwa_polecenia)
{
	for(auto it = command_list.begin(); it != command_list.end(); ++it)
		if(!strcmp(nazwa_polecenia, it->get()->GetCmdName()))
			return 1;
	return 0;
}
