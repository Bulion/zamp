
#pragma implementation
#include "RobotMobilny.hh"
#include <cassert>
#include <cmath>
#include <unistd.h>
#include <iomanip>
#include <fstream>
#include <limits>

using namespace std;

#define NAZWA_PLIKU_SCIEZKA_ROBOTA  "sciezka_robota.dat"
#define NAZWA_PLIKU_POZYCJA_ROBOTA  "pozycja_robota.dat"
#define ROZMIAR_ROBOTA              2
#define PLIK__WZORZEC_OBRYSU  		"obrys_robota.dat"

/*!
 * \brief Konstruktor klasy RobotMobilny
 */
RobotMobilny::RobotMobilny(PzG::LaczeDoGNUPlota *pPlotter): _pPlotter(pPlotter), graspper_x(0), graspper_y(0)
{
	catched_object = nullptr;
	assert( pPlotter != nullptr );
}


/*!
 * \brief Metoda przelicza i zapisuje koordynaty robota do pliku
 *
 * \retval true - jeśli operacja powiodła się,
 * \retval false - w przypadku przeciwnym.
 */
bool RobotMobilny::WriteCoord()
{
	double  x_mm, y_mm, alpha;

	Get(x_mm,y_mm,alpha);

	_Strm4Path.open(NAZWA_PLIKU_SCIEZKA_ROBOTA,ios::app);
	if (!_Strm4Path.is_open()) {
		cerr << "! Blad otwarcia pliku: " << NAZWA_PLIKU_SCIEZKA_ROBOTA << endl;
		return false;
	}
	if (!(_Strm4Path << x_mm << " " << y_mm << endl)) {
		cerr << "! Blad zapisu do pliku: " << NAZWA_PLIKU_SCIEZKA_ROBOTA << endl;
		return false;
	}
	_Strm4Path.close();

	ofstream  Strm4Position(NAZWA_PLIKU_POZYCJA_ROBOTA);
	if (!Strm4Position.is_open()) {
		cerr << "! Blad otwarcia do zapisu pliku: " << NAZWA_PLIKU_POZYCJA_ROBOTA << endl;
		return false;
	}

	ifstream  StrmWe(PLIK__WZORZEC_OBRYSU);
	double wzorcowe_x, wzorcowe_y, tmp_x, tmp_y, przekatna, kat_wzorcowy;

	alpha *= M_PI/180;
	int i = 0;
	do {
		if (StrmWe >> wzorcowe_x >> wzorcowe_y) {
			przekatna = sqrt(pow(wzorcowe_x,2)+pow(wzorcowe_y,2));
			kat_wzorcowy = atan2(wzorcowe_y,wzorcowe_x);
			tmp_x = x_mm+cos(kat_wzorcowy+alpha)*przekatna;
			tmp_y = y_mm+sin(kat_wzorcowy+alpha)*przekatna;
			if(!i)
			{
				graspper_x = tmp_x;
				graspper_y = tmp_y;
				++i;
			}
			else if (!(Strm4Position << tmp_x << " " << tmp_y << endl)) {
				cerr << "! Blad zapisu do pliku: " << NAZWA_PLIKU_POZYCJA_ROBOTA << endl;
				return false;
			}
			continue;
		}
		if(catched_object != nullptr)
		{
			ofstream stream;
			string name;
			name = catched_object->getName()+".dat";
			stream.open(name.c_str());
			if (!stream.is_open())
			{
				cerr << "Błąd otawarcia pliku " << name << endl;
				return false;
			}
			if (!(stream << graspper_x << " " << graspper_y << endl)) {
				cerr << "! Blad zapisu do pliku: " << name << endl;
				return false;
			}
			stream.close();
		}
		StrmWe.clear();
		StrmWe.ignore(numeric_limits<int>::max(),'\n');  // Przeskakuje linie
	} while (!StrmWe.eof());

	return true;
}



/*!
 * \retval true - jeśli operacja powiodła się,
 * \retval false - w przypadku przeciwnym.
 */
bool RobotMobilny::Init()
{
	assert( _pPlotter != nullptr );

	// Aby usunąć zawartość pliku
	_Strm4Path.open(NAZWA_PLIKU_SCIEZKA_ROBOTA);
	if (!_Strm4Path.is_open()) return false;
	_Strm4Path.close();
	if (!WriteCoord()) return false;
	_Strm4Path.close();
	_pPlotter->DodajNazwePliku(NAZWA_PLIKU_SCIEZKA_ROBOTA);
	_pPlotter->DodajNazwePliku(NAZWA_PLIKU_POZYCJA_ROBOTA,PzG::RR_Ciagly,
			ROZMIAR_ROBOTA);
	_pPlotter->Rysuj();
	return true;
}

/*!
 * \brief Metoda do chwytania obiektów ze sceny
 *
 * \param[in] scene - Scena z zaznaczonymi obiektami
 */
void RobotMobilny::Catch(Scene *scene)
{
	double x,y;
	if(catched_object == nullptr)
	{
		for(int i = 0; scene->objectVectorSize() > i; ++i)
		{
			scene->getObject(i)->getPosition(x,y);
			if(!(x > (int)graspper_x+10 || x < (int)graspper_x-10))
				if(!(y > (int)graspper_y+10 || y < (int)graspper_y-10))
				{
					catched_object = scene->getObject(i);
					cout << "Pomyślnie chwyciłem obiekt "
							<< scene->getObject(i)->getName() << "!" << endl;
					return;
				}
		}
		cout << "Brak obiektu w obrebie chwytaka!!!" << endl;
	}
	else
		cout << "Robot już trzyma obiekt!!!" << endl;
}

/*!
 * \brief Metoda upuszczająca podniesiony obiekt sceny
 */
void RobotMobilny::Loose()
{
	catched_object->setPosition(graspper_x,graspper_y);
	catched_object = nullptr;
}


/*!
 *
 */
bool RobotMobilny::AfterUpdate()
{
	assert( _pPlotter != nullptr );
	if (!WriteCoord()) return false;
	_pPlotter->Rysuj();
	return true;
}
