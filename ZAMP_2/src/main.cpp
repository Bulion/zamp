#include <iostream>
#include <dlfcn.h>
#include <cassert>
#include "plugin_list.hh"
#include "command_list.hh"
#include "scene.hh"

using std::cout;
using std::cerr;
using std::endl;

using namespace std;

/*!
 * \brief Metoda wyswietlająca menu główne programu
 *
 */
void WyswietlMenu()
{
	cout << endl << "w - wczytanie sekwencji instrukcji" << endl
				 << "p - pokaz sekwencje instrukcji" << endl
				 << "i - pokaz dostepne instrukcje/wtyczki" << endl
				 << "s - start wykonywania sekwencji instrukcji" << endl
				 << "a - dodaj nowa wtyczke" << endl
				 << "d - ususn wtyczke" << endl
				 << "? - wyswietl ponownie menu" << endl
				 << endl
				 << "k - koniec dzialania programu" << endl;
}

int main()
{
	PluginList lista_pluginow;
	CommandList lista_komend;
	Scene scena;
	scena.ReadFile("opis_sceny.xml");
	lista_pluginow.WczytajPlugin("command4move.so");
	lista_pluginow.WczytajPlugin("command4turn.so");
	lista_pluginow.WczytajPlugin("command4rotate.so");
	lista_pluginow.WczytajPlugin("command4grasper.so");
	lista_komend.WczytajSekwencje(&lista_pluginow,"commands.dat");
	char opcja = '?';
	do
	{
		switch(opcja)
		{
		case 'w':
			lista_komend.WczytajSekwencje(&lista_pluginow);
			break;
		case 'p':
			lista_komend.PokazSekwencje();
			break;
		case 'i':
			lista_pluginow.PokazPluginy();
			break;
		case 's':
			lista_komend.WykonajSekwencje(&scena);
			break;
		case 'a':
			lista_pluginow.WczytajPlugin();
			break;
		case 'd':
			lista_pluginow.UsunPlugin(&lista_komend);
			break;
		case '?':
			WyswietlMenu();
			break;
		default:
			cout << endl << "Wybrales zla opcje sproboj jeszcze raz!" << endl;
		}
		cout << endl << "Twoj wybor: ";
	}
	while(cin >> opcja, opcja != 'k');

	cout << endl << "Program zakonczyl dzialanie!" << endl;
}
