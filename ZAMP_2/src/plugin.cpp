/*
 * plugin.cpp
 *
 *  Created on: 7 lis 2015
 *      Author: dawid
 */

#include "plugin.hh"
#include <cstring>
#include <dlfcn.h>
#include <cassert>

using namespace std;

/*!
 * \brief Konstruktor klasy Plugin
 * Zeruje zmienne zawarte w klasie
 */
Plugin::Plugin(): Name(0), LibHnd(0) {}

/*!
 * \brief Destruktor klasy Plugin
 * Zamyka połączenie z biblioteką
 */
Plugin::~Plugin()
{
	if(LibHnd)
		dlclose(LibHnd);
}

/*!
 * \brief Metoda wczytująca bibliotekę współdzieloną o podanej nazwie
 *
 * \param[in] *plugin_name - Nazwa lub nazwa ze ścieżką do biblioteki współdzielonej
 *
 * \retval 1 Operacja zakończyła się niepowodzeniem
 * \retval 0 Operacja zakończyła się sukcesem
 */
int Plugin::LoadPlugin(const char *plugin_name)
{
	LibHnd = dlopen(plugin_name,RTLD_LAZY);
	Command *(*pCreateCmd)(void);
	void *pFun;

	if (!LibHnd) {
		cerr << "!!! Brak biblioteki: " << plugin_name << endl;
		return 1;
	}

	pFun = dlsym(LibHnd,"CreateCmd");
	if (!pFun) {
		cerr << "!!! Nie znaleziono funkcji CreateCmd" << endl;
		return 1;
	}
	pCreateCmd = *reinterpret_cast<Command* (**)(void)>(&pFun);


	Command *pCmd = pCreateCmd();

	Name = (char *)pCmd->GetCmdName();

	cout << endl << "Pomyslnie dodalem plugin " << Name << endl;

	delete pCmd;

	return 0;
}

/*!
 * \brief Metoda zwraca nazwę pluginu
 */
char * Plugin::GetName()
{
	return Name;
}

/*!
 * \brief Metoda tworząca klasę Command
 * \return Zwraca wskaźnik na nowo utworzoną klasę Command
 */
Command * Plugin::CreateCmd()
{
	void *pFun;
	Command *(*pCreateCmd)(void);
	pFun = dlsym(LibHnd,"CreateCmd");
	pCreateCmd = *reinterpret_cast<Command* (**)(void)>(&pFun);
	return pCreateCmd();
}
