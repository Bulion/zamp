/*
 * plugin_list.cpp
 *
 *  Created on: 11 lis 2015
 *      Author: dawid
 */

#include "plugin_list.hh"

using namespace std;

/*!
 * \brief Metoda pozwala na wczytanie pluginów do programu
 * Odpytuje użykownika o ścieżkę do pliku a następnie próbuje załadować ją do programu.
 * Metoda wykrywa czy operacja się nie powiodła, informując o tym użytkownika.
 */
void PluginList::WczytajPlugin()
{
	plug_ptr plug(new Plugin);
	char plugin_name[100];
	cout << endl << "Podaj nazwe pluginu: ";
	cin >> plugin_name;
if(!plug->LoadPlugin(plugin_name))
		plugin_map.insert(pair<char*,plug_ptr>(plug->GetName(),move(plug)));
	else
		cerr << endl << "Cos poszlo nie tak" << endl;
}

/*!
 * \brief Przeciąrzenie metody wczytującej pluginy.
 * Rozszerzenie metody WczytajPlugin pozwalające na podanie nazwy pluginu jako argumentu.
 * Metoda wykrywa czy operacja się nie powiodła, informując o tym użytkownika.
 * \param[in] plugin_name - nazwa pluginu do wczytania
 */
void PluginList::WczytajPlugin(const char *plugin_name)
{
	plug_ptr plug(new Plugin);
	if(!plug->LoadPlugin(plugin_name))
		plugin_map.insert(pair<char*,plug_ptr>(plug->GetName(),move(plug)));
	else
		cerr << endl << "Cos poszlo nie tak" << endl;
}

/*!
 * \brief Wyświetla aktualnie wczytane do programu pluginy
 */
void PluginList::PokazPluginy()
{
	Command *cmd;
	for(auto it = plugin_map.begin(); it != plugin_map.end(); ++it)
	{
		cmd = it->second->CreateCmd();
		cmd->PrintSyntax();
		delete cmd;
	}
}

/*!
 * \brief Metoda usuwa pluginy załadowane do programu
 * Posiada zabezpieczenie przed usunięciem używanego pluginu
 *
 * \param[in] *lista_komend - Klasa zawierająca listę wczytanych komend
 */
void PluginList::UsunPlugin(CommandList *lista_komend)
{
	char nazwa_pluginu[100];
	auto it = plugin_map.begin();
	Command *cmd;
	for(; it != plugin_map.end(); ++it)
	{
		cmd = it->second->CreateCmd();
		cmd->PrintSyntax();
		delete cmd;
	}
	if(plugin_map.begin() != plugin_map.end())
	{
		cout << endl << "Podaj nazwe pluginu do usuniecia: ";
		cin >> nazwa_pluginu;
		if(WyszukajPlugin(nazwa_pluginu))
			cout << endl << "Brak podanego pluginu na liście!" << endl;
		else if(!lista_komend->WyszukajKomende(nazwa_pluginu))
			plugin_map.erase(nazwa_pluginu);
		else
			cout << endl << "Podany plugin jest wykorzystywany i nie może zostać usunięty!" << endl;
	}
	else
		cout << endl << "Brak pluginow do usuniecia"<< endl;
}

/*!
 * \brief Metoda tworzy nowy obiekt klasy Command i zwraca wskaźnik na niego
 *
 * \param[in] *nazwa_pluginu - Nazwa pluginu z którego chcemy skorzystać
 */
Command * PluginList::UtworzKomende(char *nazwa_pluginu)
{
	return plugin_map.find(nazwa_pluginu)->second->CreateCmd();
}

/*!
 * \brief Metoda wyszukuje czy dany plugin znajduje się na liście
 *
 * \param[in] *nazwa_pluginu - Nazwa wyszukiwanego pluginu
 *
 * \retval 0 - Podany plugin znajduje się na liście
 * \retval 1 - Brak podanego pluginu na liście
 */
int PluginList::WyszukajPlugin(char *nazwa_pluginu)
{
	if(plugin_map.find(nazwa_pluginu) != plugin_map.end())
		return 0;
	else
		return 1;
}


