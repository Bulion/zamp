#ifndef  COMMAND_HH
#define  COMMAND_HH

#include <iostream>
#include "robotpose.hh"

/*!
 * \file
 * \brief Definicja klasy Command
 *
 * Plik zawiera definicję klasy Command która jest czysto wirtualna.
 * Służy ona do obsługi wczytywanych bibliotek współdzielonych. 
 */

/*!
 * \brief Modeluje abstrakcyjne polecenie dla robota mobilnego
 *
 * Klasa modeluje polecenia na podstawie wczytanych bibliotek współdzielonych.
 * Każda bibloteka posiada osobne metody realizujące odpowiadające jej zadanie
 * lecz wywoływana jest tym samym poleceniem.
 */
 class Command {
  public:
   /*!
    * \brief Destruktor wirtualny ze wzgledu na klasy pochodne
    *
    *  
    */
   virtual ~Command() {}
	/*!
	 * \brief Wyświetla nazwę polecenia wraz z wczytanymi parametrami
	 *
	 */
   virtual void PrintCmd() const = 0;
	/*!
	 *
	 * \brief Wyświetla składnie polecenia
	 */
   virtual void PrintSyntax() const = 0;
	/*!
	 * \brief Metoda zwraca nazwę polecenia
	 * \return Nazwa polecenia
	 */
   virtual const char* GetCmdName() const = 0;
	/*!
	 * \brief Metoda wykonująca zadane polecenie
	 *
	 * ExecCmd porusza przekazanym do funkcji robotem oraz informuje czy
	 * dana operacja zakończyła się sukcesem.
	 *
	 * \param[in] *pRobPose - Wskaźnik na klasę określającą orientacje robota
	 *
	 * \retval TRUE - Operacja zakończyła się sukcesem.
	 * \retval FALSE - Operacja zakończyła się porażką.
	 */
   virtual bool ExecCmd(  RobotPose  *pRobPose  ) const = 0;
	/*!
	 * \brief Metoda prarsująca wartości odpowiednie dla danego polecenia
	 *
	 * Analizuje dane otrzymane w formie strumienia wejściowego i zapisuje do odpowiednich
	 * zmiennych w klase.
	 *
	 * \param[in] Strm_CmdList - Strumień wejściowy zawierający parametry dla danego polecenia
	 *
	 * \retval TRUE - Operacja zakończyła się sukcesem.
	 * \retval FALSE - Operacja zakończyła się porażką.
	 */
   virtual bool ReadParams(std::istream& Strm_CmdsList) = 0;
 };

#endif
