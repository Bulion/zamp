#ifndef ROBOTMOBILNY_HH
#define ROBOTMOBILNY_HH

#pragma interface

/*!
 * \file
 * \brief Definicja klasy RobotPose
 *
 * Plik zawiera definicję klasy RobotPose
 */

#include "robotpose.hh"
#include "lacze_do_gnuplota.hh"
#include "scene.hh"
#include "object.hh"
#include <fstream>

/*!
 * \brief Modeluje zestaw informacji określający pozę robota
 *
 *  Klasa modeluje wirtualnego robota który porusza się według podanych
 *  przez użytkownika poleceń. Dziedziczy ona po klasie RobotPose dzięki
 *  której ma możliwość zapisywania swojego położenia.
 */
class RobotMobilny: public RobotPose {
   
  public:
  /*!
   * \brief Konstruktor
   */
   RobotMobilny(PzG::LaczeDoGNUPlota *pPlotter);
 
  /*!
   * \brief Inicjalizuje struktury robota
   */
   bool Init();

  /*!
   * \brief Resetuje pozycję robot i kasuje jego ścieżkę
   */
   bool Reset();
   /*!
    * \brief Współrzędne robota zapisane zostają do pliku
    */
   bool WriteCoord();
   /*!
    * \brief Sprawdza i chwyta obiekt na scenie
    */
   void Catch(Scene *scene);
   /*!
    * \brief Puszcza trzymany obiekt
    */
   void Loose();
   /*!
    *  Metoda wywoływana jest po aktualizacji położenia i orientacji
    *  lub samego położenia.
    *  \retval true - gdy operacja powiodła się,
    *  \retval false - w przypadku przeciwnym.
    */
   virtual bool AfterUpdate();

  private:
   /*!
    * \brief
    */
    PzG::LaczeDoGNUPlota  *_pPlotter;
   /*!
    * \brief W strumieniu zapisywana są kolejne współrzędne robota
    */
    std::ofstream   _Strm4Path;
    /*!
     * \brief wskaźnik na chwycony obiekt
     */
    SceneObject *catched_object;
    /*!
     * \brief Wymiar robota w osi x podany w mm
     */
    double graspper_x;
    /*!
     * \brief Wymiar robota w osi y podany w mm
     */
    double graspper_y;

};


#endif
