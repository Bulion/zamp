/*
 * command_list.hh
 *
 *  Created on: 11 lis 2015
 *      Author: dawid
 */

#ifndef INC_COMMAND_LIST_HH_
#define INC_COMMAND_LIST_HH_

#include <memory>
#include "plugin_list.hh"
#include "command.hh"
#include "scene.hh"

class PluginList;

/*!
 * \file
 * \brief Definicja klasy Command
 *
 * Wykorzystuje ona kontener STL przechowujący inteligentny wskaźnik na daną komendę
 */
typedef std::unique_ptr<Command> cmd_ptr;

/*!
 * \brief Tworzy listę komend
 *
 * Pozwala na manipulację znajdującymi się w niej komendami oraz uruchomienie
 * sekwencyjnego ich wykonywania.
 */
class CommandList
{
	/*!
	 * \brief Kontener STL zawierający listę wskaźników inteligętnych (unique_ptr)
	 * na klasy Command
	 */
	std::list<cmd_ptr> command_list;
public:
	int WyszukajKomende(char *nazwa_polecenia);
	void WczytajSekwencje(PluginList *lista_pluginow);
	void WczytajSekwencje(PluginList *lista_pluginow, const char *);
	void PokazSekwencje();
	void WykonajSekwencje(Scene *scena);
};


#endif /* INC_COMMAND_LIST_HH_ */
