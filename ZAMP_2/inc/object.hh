/*
 * object.hh
 *
 *  Created on: 4 gru 2015
 *      Author: dawid
 */

#ifndef INC_OBJECT_HH_
#define INC_OBJECT_HH_

#include <string>

/*!
 * \brief Klasa opisująca obiekt znajdujący się na scenie
 */

class SceneObject
{
	/*!
	 * \brief Nazwa obiektu
	 */
	std::string object_name;
	/*!
	 * \brief Pozycja obiektu na scenie w osi x w mm
	 */
	double x_mm;
	/*!
	 * \brief Pozycja obiektu na scenie w osi y w mm
	 */
	double y_mm;
	/*!
	 * \brief Rozmiar obiektu
	 */
	int size;
public:
	/*!
	 * \brief Konstruktor obiektu
	 */
	SceneObject(): x_mm(0), y_mm(0), size(0) {}
	/*!
	 * \brief Metoda manipulująca nazwą obiektu
	 *
	 * \param[in] name - nazwa obiektu
	 */
	void setName(char *name) {object_name = name;}
	/*!
	 * \brief Przeciąrzenie metody manipulującej nazwą obiektu dla typu string
	 *
	 * \param[in] name - nazwa obiektu
	 */
	void setName(std::string name) {object_name = name;}
	/*!
	 * \brief Metoda manipulująca położeniem obiektu
	 *
	 * \param[in] x_mm - położenie obiektu w osi x
	 * \param[in] y_mm - położenie obiektu w osi y
	 */
	void setPosition(double x_mm, double y_mm) { SceneObject::x_mm = x_mm;
												 SceneObject::y_mm = y_mm; }
	/*!
	 * \brief Metoda manipulująca wielkością obiektu
	 *
	 * \param[in] size - wielkość obiektu
	 */
	void setSize(int size) { SceneObject::size = size; }
	/*!
	 * \brief Metoda pozwalająca na otrzymanie nazwy obiektu
	 *
	 * \return string z nazwą
	 */
	std::string getName() { return object_name; }
	/*!
	 * \brief Metoda zwracająca położenie obiektu
	 *
	 * \param[out] x_mm - położenie obiektu w osi x
	 * \param[out] y_mm - położenie obiektu w osi y
	 */
	void getPosition(double &x_mm, double &y_mm) { x_mm = SceneObject::x_mm;
												   y_mm = SceneObject::y_mm;}
	/*!
	 * \brief Metoda zwracająca wielkość obiektu
	 *
	 * \param[in] size - wielkość obiektu
	 */
	int getSize() { return size; }
};


#endif /* INC_OBJECT_HH_ */
