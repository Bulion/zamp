/*
 * plugin.hh
 *
 *  Created on: 7 lis 2015
 *      Author: dawid
 */

#ifndef INC_PLUGIN_HH_
#define INC_PLUGIN_HH_

#include <map>
#include <list>
#include <memory>
#include <cstring>
#include "command.hh"

/*!
 * \file
 * \brief Definicja klasy Plugin oraz PluginList
 *
 */

/*!
 * \brief Modeluje podstawowe metody pozwalające zarządzać pluginami
 */
class Plugin{
	/*!
	 * \brief Przetrzymuje nazwę pluginu
	 */
	char *Name;
	/*!
	 * \brief Wskaźnik na uchwyt wczytanej biblioteki.
	 */
	void *LibHnd;
public:
	Plugin();
	~Plugin();
	int LoadPlugin(const char *plugin_name);
	char * GetName();
	Command * CreateCmd();
};

#endif /* INC_PLUGIN_HH_ */
