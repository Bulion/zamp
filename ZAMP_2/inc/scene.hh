#ifndef SCENE_HH
#define SCENE_HH

/*!
 * \file
 * \brief Tu trzeba napisać własny opis
 *
 * Tu trzeba napisać własny opis
 */

#include <vector>
#include "object.hh"


/*!
 * \brief Klasa Scene przetrzymuje informacje na temat wyglądu sceny
 *
 * Pozwala również na wczytanie obiektów z pliku XML.
 */
class Scene {
	/*!
	 * \brief Wektor obiektów znajdujących się na scenie
	 */
	std::vector<SceneObject> ObjectVector;
	/*!
	 * \brief Zapamięstana pozycja robota w osi x w mm
	 */
	double robot_x_pose;
	/*!
	 * \brief Zapamięstana pozycja robota w osi y w mm
	 */
	double robot_y_pose;
	/*!
	 * \brief Zapamiętany kąt obrotu robota w stopniach
	 */
	double robot_angle_pose;
public:
	bool ReadFile(const char* sFileName);
	void setRobotPose(double x, double y, double angle);
	void getRobotPose(double &x, double &y, double &angle);
	void addObject(SceneObject *Object);
	/*!
	 * \brief Zwraca najnowszy obiekt dodany do wektora
	 *
	 * \return Najnowszy obiekt wektora
	 */
	SceneObject getNewestObject() { return ObjectVector.back(); }
	/*!
	 * \brief Zwraca wskazany przez zmienną obiekt
	 *
	 * \param[in] i - numer obiektu
	 *
	 * \return obiekt znajdujący się na scenie
	 */
	SceneObject * getObject(int i) { return &(ObjectVector[i]); }
	int objectVectorSize();

};


#endif
