var class_scene_object =
[
    [ "SceneObject", "class_scene_object.html#a0d268d96d77dbeb45b07a6442e2f4d0d", null ],
    [ "getName", "class_scene_object.html#a2bda4e56bbeddcd57ac696261dcd6fd3", null ],
    [ "getPosition", "class_scene_object.html#a08c3caf751abdee31e75a1f442028e9a", null ],
    [ "getSize", "class_scene_object.html#aeb6295edeae0ab8900eb53506ae852fb", null ],
    [ "setName", "class_scene_object.html#a420d7a26978ac0a77f7b7af4bb217331", null ],
    [ "setName", "class_scene_object.html#ad8ac9406b35bee3cd4fad75db013d9c4", null ],
    [ "setPosition", "class_scene_object.html#a61ff1e35a24b1fb4ee425ff25a0ddef6", null ],
    [ "setSize", "class_scene_object.html#a59ef24482eb00f71e796a765bc369d87", null ],
    [ "object_name", "class_scene_object.html#a47b483ce7d0afc7a83c1766a1a3fa0b4", null ],
    [ "size", "class_scene_object.html#a882770f2dda4ca70da5ede5041319221", null ],
    [ "x_mm", "class_scene_object.html#a5fca62418e0b3dbe8dfc71d33b8394b4", null ],
    [ "y_mm", "class_scene_object.html#a234764e1a614aa5a1f4f3b4594022456", null ]
];