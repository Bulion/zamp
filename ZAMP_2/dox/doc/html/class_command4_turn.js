var class_command4_turn =
[
    [ "Command4Turn", "class_command4_turn.html#a7e0353961654a8a337be55c24c3d3896", null ],
    [ "ExecCmd", "class_command4_turn.html#a6f2f31e8c65006e3d21ce925d4872293", null ],
    [ "GetCmdName", "class_command4_turn.html#ac946f08b2bec0b01eb398a980367ebf8", null ],
    [ "PrintCmd", "class_command4_turn.html#a38edb94514a3445979d932d9fbc768cd", null ],
    [ "PrintSyntax", "class_command4_turn.html#a7ea3231d18a5e399ec7113bb59637565", null ],
    [ "ReadParams", "class_command4_turn.html#a9ee87ffe0ef0bc5496bda60990a2220b", null ],
    [ "_Dist_mm", "class_command4_turn.html#a1b8c74db820e11818bba9c9f0dfcbd11", null ],
    [ "_Radius_mm", "class_command4_turn.html#a6fd0ea986daff004597babbbb2d86a01", null ],
    [ "_Speed_mmS", "class_command4_turn.html#a17b9a2cc04befadb5760fc66f97d7af2", null ]
];