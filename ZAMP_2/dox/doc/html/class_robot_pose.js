var class_robot_pose =
[
    [ "RobotPose", "class_robot_pose.html#a9c11b53a65ec4e7ab215d33acfdf4c4f", null ],
    [ "AfterUpdate", "class_robot_pose.html#a312fc928d371ed2713ed0d1bcd3f5db2", null ],
    [ "Get", "class_robot_pose.html#af70dd87fd8e88fb235473b5295754dae", null ],
    [ "Get", "class_robot_pose.html#a6126027f56cd86bb96dfa100e0e147b4", null ],
    [ "GetGrasper", "class_robot_pose.html#a8140a3b60b55c67c8975a304cab4645e", null ],
    [ "Set", "class_robot_pose.html#a1c8b4c796e03c5172c65de5e2fd24393", null ],
    [ "Set", "class_robot_pose.html#a67ed67edeb4b54169a30728b6503a7a7", null ],
    [ "SetAlpha", "class_robot_pose.html#a3053974edd9dc3e74d23330397ddb3dd", null ],
    [ "SetGrasper", "class_robot_pose.html#a9a52f810ece5fc47b64255818ff46762", null ],
    [ "alpha_deg", "class_robot_pose.html#aae28f25b1976058f5cc54103949b097c", null ],
    [ "grasper_state", "class_robot_pose.html#a15f9769b21bd8c8764d29268a73aa02e", null ],
    [ "x_mm", "class_robot_pose.html#ac1aa2c2cb00c28c4c5e7822e46e5df13", null ],
    [ "y_mm", "class_robot_pose.html#a46cfcb519d857ef57a0c40c2dee96403", null ]
];