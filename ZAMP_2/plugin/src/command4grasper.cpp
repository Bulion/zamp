#include <iostream>
#include <cmath>
#include <unistd.h>
#include "command4grasper.hh"

using std::cout;
using std::endl;

#define RES 100

extern "C" {
Command* CreateCmd(void);
}




/*!
 * \brief
 *
 *
 */
Command* CreateCmd(void)
{
	return Command4Grasper::CreateCmd();
}


/*!
 *
 */
Command4Grasper::Command4Grasper(): _state(0)
{}


/*!
 *
 */
void Command4Grasper::PrintCmd() const
{
	cout << GetCmdName() << " " << _state << endl;
}


/*!
 *
*/
const char* Command4Grasper::GetCmdName() const
{
	return "Grasper";
}


/*!
 *
 */
bool Command4Grasper::ExecCmd( RobotPose     *pRobPose ) const
{
	pRobPose->SetGrasper(_state);
	return true;
}


/*!
 *
 */
bool Command4Grasper::ReadParams(std::istream& Strm_CmdsList)
{
	Strm_CmdsList >> _state;
	if(!(_state == 0 || _state == 1))
	{
		std::cerr << endl << "Chwytak ma tylko dwa stany (0 - otwarty, 1 - zamkniety)!" << endl;
		return false;
	}
	if(Strm_CmdsList.fail())
		return false;
	return true;
}


/*!
 *
*/
Command* Command4Grasper::CreateCmd()
{
	return new Command4Grasper();
}


/*!
 *
 */
void Command4Grasper::PrintSyntax() const
{
	cout << "   Grasper state[0/1]" << endl;
}
