#ifndef  COMMAND4MOVE_HH
#define  COMMAND4MOVE_HH

#ifndef __GNUG__
# pragma interface
# pragma implementation
#endif

#include "command.hh"
#include <sstream>

/*!
 * \file
 * \brief Definicja klasy Command4Grasper
 *
 * Plik zawiera definicję klasy Command4Grasper
 */

/*!
 * \brief Modeluje polecenie dla robota mobilnego, które wymusza jego ruch do przodu
 *
 *  Dostarcza metody pozwalające na przemieszczanie się robotem do przodu.
 */
class Command4Grasper: public Command {
	/*!
	 * \brief Określa stan chwytaka
	 * Chwytak zaciśnięty 1, w przeciwnym wypadku 0
	 */
	int  _state;
public:
	/*!
	 * \brief Konstruktor zerujący wartości klasy
	 */
	Command4Grasper();
	/*!
	 * \brief Wyświetla nazwę polecenia wraz z wczytanymi parametrami
	 *
	 */
	virtual void PrintCmd() const;
	/*!
	 *
	 * \brief Wyświetla składnie polecenia
	 */
	virtual void PrintSyntax() const;
	/*!
	 * \brief Metoda zwraca nazwę polecenia
	 * \return Nazwa polecenia
	 */
	virtual const char* GetCmdName() const;
	/*!
	 * \brief Metoda wykonująca zadane polecenie
	 *
	 * ExecCmd porusza przekazanym do funkcji robotem oraz informuje czy
	 * dana operacja zakończyła się sukcesem.
	 *
	 * \param[in] *pRobPose - Wskaźnik na klasę określającą orientacje robota
	 *
	 * \retval TRUE - Operacja zakończyła się sukcesem.
	 * \retval FALSE - Operacja zakończyła się porażką.
	 */
	virtual bool ExecCmd(  RobotPose  *pRobPose  ) const;
	/*!
	 * \brief Metoda prarsująca wartości odpowiednie dla danego polecenia
	 *
	 * Analizuje dane otrzymane w formie strumienia wejściowego i zapisuje do odpowiednich
	 * zmiennych w klase.
	 *
	 * \param[in] Strm_CmdList - Strumień wejściowy zawierający parametry dla danego polecenia
	 *
	 * \retval TRUE - Operacja zakończyła się sukcesem.
	 * \retval FALSE - Operacja zakończyła się porażką.
	 */
	virtual bool ReadParams(std::istream& Strm_CmdsList);
	/*!
	 * \brief Metoda tworząca nowy obiekt klasy Command i zwraca wskaźnik na niego
	 *
	 * \return Wskaźnik na klasę typu Command
	 */
	static Command* CreateCmd();
};

#endif
