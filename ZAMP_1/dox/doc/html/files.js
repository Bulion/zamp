var files =
[
    [ "command.hh", "command_8hh.html", [
      [ "Command", "class_command.html", "class_command" ]
    ] ],
    [ "command4move.cpp", "command4move_8cpp.html", "command4move_8cpp" ],
    [ "command4move.hh", "command4move_8hh.html", [
      [ "Command4Move", "class_command4_move.html", "class_command4_move" ]
    ] ],
    [ "command4rotate.cpp", "command4rotate_8cpp.html", "command4rotate_8cpp" ],
    [ "command4rotate.hh", "command4rotate_8hh.html", [
      [ "Command4Rotate", "class_command4_rotate.html", "class_command4_rotate" ]
    ] ],
    [ "command4turn.cpp", "command4turn_8cpp.html", "command4turn_8cpp" ],
    [ "command4turn.hh", "command4turn_8hh.html", [
      [ "Command4Turn", "class_command4_turn.html", "class_command4_turn" ]
    ] ],
    [ "command_list.cpp", "command__list_8cpp.html", null ],
    [ "command_list.hh", "command__list_8hh.html", "command__list_8hh" ],
    [ "lacze_do_gnuplota.cpp", "lacze__do__gnuplota_8cpp.html", "lacze__do__gnuplota_8cpp" ],
    [ "lacze_do_gnuplota.hh", "lacze__do__gnuplota_8hh.html", "lacze__do__gnuplota_8hh" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "plugin.cpp", "plugin_8cpp.html", null ],
    [ "plugin.hh", "plugin_8hh.html", [
      [ "Plugin", "class_plugin.html", "class_plugin" ]
    ] ],
    [ "plugin_list.cpp", "plugin__list_8cpp.html", null ],
    [ "plugin_list.hh", "plugin__list_8hh.html", "plugin__list_8hh" ],
    [ "RobotMobilny.cpp", "_robot_mobilny_8cpp.html", "_robot_mobilny_8cpp" ],
    [ "RobotMobilny.hh", "_robot_mobilny_8hh.html", [
      [ "RobotMobilny", "class_robot_mobilny.html", "class_robot_mobilny" ]
    ] ],
    [ "robotpose.hh", "robotpose_8hh.html", [
      [ "RobotPose", "class_robot_pose.html", "class_robot_pose" ]
    ] ],
    [ "strony_dokumentacji.page", "strony__dokumentacji_8page.html", null ]
];