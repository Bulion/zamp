#include <iostream>
#include <cmath>
#include <unistd.h>
#include "command4turn.hh"

using std::cout;
using std::endl;

#define RES 100

extern "C" {
Command* CreateCmd(void);
}




/*!
 * \brief
 *
 *
 */
Command* CreateCmd(void)
{
	return Command4Turn::CreateCmd();
}


/*!
 *
 */
Command4Turn::Command4Turn(): _Speed_mmS(0), _Dist_mm(0), _Radius_mm(0)
{}


/*!
 *
 */
void Command4Turn::PrintCmd() const
{
	cout << GetCmdName() << " " << _Speed_mmS  << " " << _Dist_mm << " " << _Radius_mm << endl;
}


/*!
 *
 */
const char* Command4Turn::GetCmdName() const
{
	return "Turn";
}


/*!
 *
 */
bool Command4Turn::ExecCmd( RobotPose     *pRobPose ) const
{
	int i;
	int travel_time_MS = std::abs((_Dist_mm/_Speed_mmS)*1000);
	int resolution = (travel_time_MS/RES);
	double x_mm, y_mm, alpha_deg, alpha_rad;
	double rotate_angle_step;
	double circle_center_x, circle_center_y;
	pRobPose->Get(x_mm,y_mm,alpha_deg);
	if(alpha_deg > 180)
		alpha_deg -= 360;
	if(alpha_deg < -180)
		alpha_deg += 360;
	alpha_rad = alpha_deg*M_PI/180;

	circle_center_x = sin(alpha_rad)*_Radius_mm;
	circle_center_y = -cos(alpha_rad)*_Radius_mm;

	circle_center_x = x_mm - circle_center_x;
	circle_center_y = y_mm - circle_center_y;

	rotate_angle_step = ((_Dist_mm*360)/(2*M_PI*_Radius_mm))/RES;
	for(i = 0; i < travel_time_MS; i+=resolution)
	{
		if(_Speed_mmS > 0)
			alpha_deg += rotate_angle_step;
		else
			alpha_deg -= rotate_angle_step;
		if(alpha_deg > 180)
			alpha_deg -= 360;
		if(alpha_deg < -180)
			alpha_deg += 360;
		alpha_rad = alpha_deg*M_PI/180;
		if(alpha_deg < 0)
			alpha_deg += 360;
		x_mm = circle_center_x + sin(alpha_rad)*_Radius_mm;
		y_mm = circle_center_y - cos(alpha_rad)*_Radius_mm;
		if(!pRobPose->Set(x_mm,y_mm,alpha_deg))
			return false;
		usleep(1000000/RES);
	}
	return true;
}


/*!
 *
 */
bool Command4Turn::ReadParams(std::istream& Strm_CmdsList)
{
	Strm_CmdsList >> _Speed_mmS >> _Dist_mm >> _Radius_mm;
	if(_Speed_mmS == 0)
	{
		std::cerr << endl << "Predkosc musi byc rozna od 0!" << endl;
		return false;
	}
	if(_Dist_mm <= 0)
	{
		std::cerr << endl << "Droga musi byc wieksza niz 0!" << endl;
		return false;
	}
	if(_Radius_mm == 0)
	{
		std::cerr << endl << "Promien musi byc rozny od 0!" << endl;
		return false;
	}
	if(Strm_CmdsList.fail())
		return false;
	return true;
}


/*!
 *
 */
Command* Command4Turn::CreateCmd()
{
	return new Command4Turn();
}


/*!
 *
 */
void Command4Turn::PrintSyntax() const
{
	cout << "   Turn speed[mm/s] dist[mm] radius[mm]" << endl;
}
