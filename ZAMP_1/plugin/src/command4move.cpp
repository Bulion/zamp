#include <iostream>
#include <cmath>
#include <unistd.h>
#include "command4move.hh"

using std::cout;
using std::endl;

#define RES 100

extern "C" {
Command* CreateCmd(void);
}




/*!
 * \brief
 *
 *
 */
Command* CreateCmd(void)
{
	return Command4Move::CreateCmd();
}


/*!
 *
 */
Command4Move::Command4Move(): _Speed_mmS(0), _Distance_mm(0)
{}


/*!
 *
 */
void Command4Move::PrintCmd() const
{
	cout << GetCmdName() << " " << _Speed_mmS  << " " << _Distance_mm << endl;
}


/*!
 *
*/
const char* Command4Move::GetCmdName() const
{
	return "Move";
}


/*!
 *
 */
bool Command4Move::ExecCmd( RobotPose     *pRobPose ) const
{
	int i;
	int travel_time_MS = std::abs((_Distance_mm/_Speed_mmS)*1000);
	int resolution = (travel_time_MS/RES);
	double x_mm, y_mm, alpha_deg, alpha_rad;
	double travel_x_step, travel_y_step;

	pRobPose->Get(x_mm,y_mm,alpha_deg);

	if(alpha_deg > 180)
		alpha_deg -= 360;
	alpha_rad = alpha_deg*M_PI/180;
	travel_x_step = cos(alpha_rad)*_Speed_mmS/1000*resolution;
	travel_y_step = sin(alpha_rad)*_Speed_mmS/1000*resolution;
	for(i = 0; i < travel_time_MS; i+=resolution)
	{
		x_mm += travel_x_step;
		y_mm += travel_y_step;
		if(!pRobPose->Set(x_mm,y_mm))
			return false;
		usleep(1000000/RES);
	}
	return true;
}


/*!
 *
 */
bool Command4Move::ReadParams(std::istream& Strm_CmdsList)
{
	Strm_CmdsList >> _Speed_mmS >> _Distance_mm;
	if(_Speed_mmS == 0)
	{
		std::cerr << endl << "Predkosc musi byc rozna od 0!" << endl;
		return false;
	}
	if(_Distance_mm <= 0)
	{
		std::cerr << endl << "Droga musi byc wieksza niz 0!" << endl;
		return false;
	}
	if(Strm_CmdsList.fail())
		return false;
	return true;
}


/*!
 *
*/
Command* Command4Move::CreateCmd()
{
	return new Command4Move();
}


/*!
 *
 */
void Command4Move::PrintSyntax() const
{
	cout << "   Move speed[mm/s] dist[mm/s]" << endl;
}
