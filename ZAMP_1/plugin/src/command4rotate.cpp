#include <iostream>
#include <cmath>
#include <unistd.h>
#include "command4rotate.hh"

using std::cout;
using std::endl;

#define RES 100

extern "C" {
Command* CreateCmd(void);
}




/*!
 * \brief
 *
 *
 */
Command* CreateCmd(void)
{
	return Command4Rotate::CreateCmd();
}


/*!
 *
 */
Command4Rotate::Command4Rotate(): _Ang_speed_degS(0), _Ang_deg(0)
{}


/*!
 *
 */
void Command4Rotate::PrintCmd() const
{
	cout << GetCmdName() << " " << _Ang_speed_degS  << " " << _Ang_deg << endl;
}


/*!
 *
 */
const char* Command4Rotate::GetCmdName() const
{
	return "Rotate";
}


/*!
 *
 */
bool Command4Rotate::ExecCmd( RobotPose     *pRobPose ) const
{
	int i;
	int rotate_time_MS = std::abs((_Ang_deg/_Ang_speed_degS)*1000);
	int resolution = rotate_time_MS/RES;
	double x_mm, y_mm, alpha_deg;
	pRobPose->Get(x_mm,y_mm,alpha_deg);
	double rotate_angle_step = _Ang_speed_degS/1000*resolution;
	for(i = 0; i < rotate_time_MS; i+=resolution)
	{
		alpha_deg += rotate_angle_step;
		if(alpha_deg > 180)
			alpha_deg -= 360;
		if(alpha_deg < -180)
			alpha_deg += 360;
		if(alpha_deg < 0)
			alpha_deg += 360;
		if(!pRobPose->Set(x_mm,y_mm,alpha_deg))
			return false;
		usleep(1000000/RES);
	}
	return true;
}


/*!
 *
 */
bool Command4Rotate::ReadParams(std::istream& Strm_CmdsList)
{
	Strm_CmdsList >> _Ang_speed_degS >> _Ang_deg;
	if(_Ang_speed_degS == 0)
	{
		std::cerr << endl << "Predkosc katowa musi byc rozna od 0!" << endl;
		return false;
	}
	if(_Ang_deg <= 0)
	{
		std::cerr << endl << "Kat obrotu musi byc wiekszy od 0!" << endl;
		return false;
	}
	if(Strm_CmdsList.fail())
		return false;
	return true;
}


/*!
 *
 */
Command* Command4Rotate::CreateCmd()
{
	return new Command4Rotate();
}


/*!
 *
 */
void Command4Rotate::PrintSyntax() const
{
	cout << "   Rotate ang_speed[deg/s] ang[deg]" << endl;
}
