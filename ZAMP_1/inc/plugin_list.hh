/*
 * plugin_list.hh
 *
 *  Created on: 11 lis 2015
 *      Author: dawid
 */

#ifndef INC_PLUGIN_LIST_HH_
#define INC_PLUGIN_LIST_HH_

#include <memory>
#include "plugin.hh"
#include "plugin_list.hh"
#include "command_list.hh"

typedef std::unique_ptr<Plugin> plug_ptr;

class CommandList;

/*!
 * \brief Struktura porównująca napisy w mapie pluginów
 */
struct cmp_str
{
   bool operator()(char const *a, char const *b)
   {
      return strcmp(a, b) < 0;
   }
};

/*!
 * \brief Klasa zawierająca Mapę pluginów
 * Pozwala na dodawania usuwanie oraz wyświetlanie dostępnych pluginów.
 * Dodatkowo jest w stanie tworzyć obiekty klasy Command.
 */
class PluginList
{
	/*!
	 * \brief Kontener STL zawierający mapę pluginów
	 * Kluczem w mapie jest nazwa pluginu natomiast wartością inteligętny
	 * wskaźnik na wczytany plugin
	 */
	std::map<char*,plug_ptr,cmp_str> plugin_map;
public:
	void WczytajPlugin();
	void UsunPlugin(CommandList *lista_komend);
	void PokazPluginy();
	Command * UtworzKomende(char *nazwa_pluginu);
	int WyszukajPlugin(char *nazwa_pluginu);
};




#endif /* INC_PLUGIN_LIST_HH_ */
